# Express Auto Route

An express auto route module which allows routes and middleware to be configured via config file.

## Setup

The express auto route is fairly simple to setup. It takes an instance of express server,
an array of route configs, and optional array of middleware.

It comes with some default middleware for json schema validation which you can choose to ignore.
Our setup example will implement those middleware in the setup.

### Step 1

Include the express-auto-route module. Depending on which module system you're using, you can set it up a few ways.
We export both the namespaced module and the default for convenience.

~note~: RouteRequestJsonSchemaValidator and RouteResponseJsonSchemaValidator middleware are optional if you 
do not need schema validation. You can mix and match as you please or even write your own.

```js
// commonjs
const {AutoRoute, RouteRequestJsonSchemaValidator, RouteResponseJsonSchemaValidator} = require('express-auto-route');

// or es6 syntax
import {
    AutoRoute,
    RouteRequestJsonSchemaValidator,
    RouteResponseJsonSchemaValidator,
    Route,
    Methods
} from "@dechamp/express-auto-route";

// or es6 syntax with default
import AutoRoute, {
    RouteRequestJsonSchemaValidator,
    RouteResponseJsonSchemaValidator,
    Route,
    Methods
} from "@dechamp/express-auto-route";
```


```js
//express server
const server = ...;
// see exmple below.
const routes = ...;

AutoRoute({
  server,
  routes,
  middlewares: [
    RouteRequestJsonSchemaValidator,
    RouteResponseJsonSchemaValidator,
    // your custom middleware
  ],
});
```

### Step 2

Setup a route. There are advanced options for routes which we'll show further down but for now let's just do the 
basics. 

#### Route config example

If you wanted to add a "home" route then below is an example of it.

```js
const routes = [
    {
        method: "GET",
        modulePath: "/routes/home/get.js",
        path: "/home"
    },
    // ...
]
```

- ~method~: see exported typescript enum, Methods. or quick reference below. This would be what you
would use for the express route method. 
    - GET
    - POST
    - HEAD
    - PUT
    - DELETE
    - USE
    - if you need one not listed, please email me or submit a merge request
- ~modulePath~ <OPTIONAL>: the path to the express module (req, res, next) => ..., this can be async as well.
Typically follows "routes/<route name>/<method>" and extension is optional just like with normal module import would be.

If expcluded, then auto-route will expect a certain structure. See below for details.

- ~path~: This is your url path. example, "/home" points to https://localhost/home. You can refer to express path, which inturn uses [path-to-regex](https://www.npmjs.com/package/path-to-regexp)

**UPDATE**
`modulePath` is now optional. If you exclude it, autoroute will fallback to a default file structure.

`<subfolder for each directory name within "path">/<method>`

##### Quick example

`GET:/home` expects `<routes>/home/get.ts`
`GET:/user/profile/:first_name/test` expects `<routes>/user/profile/firstName/test/get.ts`

##### Detailed Example

The below route would have the following file structure.

```js
    // Notice that basePath is set to '/api', this will NOT affect your file structure
    await AutoRoute({
        basePath: '/api',
        // This is where we start the auto file structure from
        routeDirectory: './src/routes',
        server,
        routes: config.routes as Route[]
    });

    // route
    {
        method: "GET",
        path: "/home"
    }


    // ./src/routes/home/get.ts
    export default function get(req, res, next) {
         // Your code...
    }

    // OR

    const get = (req, res, next) => {};
    export default get;
```

The structure would be the following...

- /src
  - /routes <~ this is determined via the property `routeDirectory` set on the AutoRoute class;
    - /home
      - get.ts

#### Route Middleware

Route middleware can be setup in two ways. There is the option to apply middleware to all routes, when adding it via initialization of the AutoRoute,
 then there is the option to add middleware via the route itself which applies to only the single route.

Using the same example as above, we'll setup the route middleware using the "apply to all" option.

**Note** If you use the 'RouteRequestJsonSchemaValidator' and 'RouteResponseJsonSchemaValidator' middleware, they require you add in the json middleware for express, to convert params to json, via `server.use(express.json());`

```js
// see other examples above if you use es6 "import" method.
const {AutoRoute, RouteRequestJsonSchemaValidator, RouteResponseJsonSchemaValidator} = require('express-auto-route');

AutoRoute({
  server,
  routes,
  middlewares: [ // These apply to all routes, and enable more options per route
    RouteRequestJsonSchemaValidator,
    RouteResponseJsonSchemaValidator,
    // your custom middleware
  ],
});
```

If you wanted to add a "resource" route with json schema validation using the 'RouteResponseJsonSchemaValidator' middlware, then below is an example of it. 
This route has a response json schema validation by adding `responseJsonSchema` option to each route. This option gets enabled via the 'RouteResponseJsonSchemaValidator' 

~note~: Please refer to docs/schemaExamples for real schemas used in our examples below.

```js
const routes = [
    {
        method: "GET",
        modulePath: "/routes/resource/get.js", //optional, allows custom structure
        path: "/resources",
        responseJsonSchema: "/path/to/schemas/resources.schema.json", // this only works due to using the RouteResponseJsonSchemaValidator middleware
        middleware: [] // an array of paths to middleware that applies to this route only. This is a standard express middleware (req, res, next) => {}
    },
    // ...
]
```

We can add another route to retrieve an single resource by a uuid, by adding this to the array of routes. We will
add validation to the request query by using the `requestJsonSchema.query` option, which was enabled via 'RouteRequestJsonSchemaValidator' middleware.

```js
const routes = [
    //previous route added previously... ,
    {
        method: "GET",
        modulePath: "/routes/resource/get.js",
        path: "/resources/:uuid",
        requestJsonSchema: {
            query: "/path/to/schemas/getByUuid.schema.json" // This key was made available via 'RouteRequestJsonSchemaValidator'
        },
        responseJsonSchema: "/path/to/schemas/resource.schema.json"
    }
]
```

We can also run validation against the path params such as `:name`, using the `requestJsonSchema.params` option, which was enabled via `RouteRequestJsonSchemaValidator` middleware.
You can validate any path params using this methods, as it runs against the req.params. See the express path/path-to-regex npm modules as mentioned above, to see what populates for the params.

```js
const routes = [
    //previous route added previously... ,
    {
        method: "GET",
        modulePath: "/routes/resource/get.js",
        path: "/resources/:uuid",
        requestJsonSchema: {
            params: "/path/to/schemas/params.schema.json" // This key was made available via 'RouteRequestJsonSchemaValidator'
        },
        responseJsonSchema: "/path/to/schemas/resource.schema.json"
    }
]
```

We have the option to validate against the request body as well. Just by providing a path  to a schema file via `requestJsonSchema.body`, which again was made available via 'RouteRequestJsonSchemaValidator'

```js
const routes = [
    //previous route added previously... ,
    {
        method: POST,
        path: "/resources",
        modulePath: "/routes/resource/post.js",
        requestJsonSchema: {
            body: "/path/to/schemas/resource.schema.json" // This was made available via 'RouteRequestJsonSchemaValidator'
        },
        responseJsonSchema: "/path/to/schemas/resource.schema.json"
    }
]
```

As mentioned above, we can add middleware to a single route if we wanted as well, via the "middleware" config within each route object. This is an array, so you can add as many as you want.
For brevity, we'll just add an inline middleware that console logs a message. 

```js
const routes = [
    //previous route added previously... ,
    {
        method: "GET",
        path: "/home",
        modulePath: "/routes/home/post.js", // extension .js is not needed.
        middleware: [
          "src/middleware/logger" // this path is based off the root directory of the application, typically where you would find the package.json file
        ] 
    }
]
```

## Advanced configs

### Properties

#### ~~routeBasePath~~

deprecated, replaced with `routeDirectory`

#### routeDirectory

Specifies which folder is to be condsidered your root for all routes defined. If omitted, your project root will be used. 

It's recommend you set this. If you typically use a `src` folder, then you could set it to `src/routes`, and place all your routes in this folder.

##### Example
 
```js
const app = //express instance
const routes = { /*...*/ };
AutoRoute({
    server: app,
    basePath: '/api', // starts all routes prepended with /api
    routeDirectory: `./routes`,
    routes,
    middlewares: [
        RouteRequestJsonSchemaValidator,
        RouteResponseJsonSchemaValidator
    ]
});
```

Now we can shorten up the routes, removing the route base path we declared above. 

```js
// We will go from this...
const routes = [
    {
        method: "GET",
        path: "/home",
        modulePath: "/routes/home/post.js", // extension .js is not needed.
        middleware: [
           "/routes/home/middleware.js" // extension .js is not needed.
        ] 
    }
]

// to this, removing the /routes and while we are at it remove the extensions.
const routes = [
    {
        method: "GET",
        path: "/home",
        modulePath: "/home/post",
        middleware: [
           "/home/middleware"
        ] 
    }
]
```

#### basePath

**UPDATE** This is a new property.

`basePath` allows you to setup the base path of all routes, such as `/api` or `/v2`. If you set it, it will affect only the path but NOT the
folder structure when excluding the route property `modulePath`. 

Example

With `basePath` of value `/api`

    `/api/user`
    
Without `basePath`

    `/user`