import * as  path from 'path';
import moduleImporter from '../../../src/moduleImporter';
import appRoot from 'app-root-path';

it('will import module', async () => {
    const callback = await moduleImporter(`${path.relative(appRoot.toString(), __dirname)}/middleware.ts`);
    const req = {
        json: jest.fn()
    };
    const res = {};
    const next = () => jest.fn();

    await callback(req, res, next);
    expect(req.json.mock.calls[0][0]).toEqual({response: 'ran'});
});
