import joi from "@hapi/joi";
import moduleImporter from "./moduleImporter";
import { join } from "path";
import { RequestParamHandler } from "express";
import _ from 'lodash';
import assert from "assert";

export interface AutoRoutePropsConfigs {
    basePath?: string;
    routeDirectory?: string;
    // Deprecated
    routeBasePath?: string;
}

export interface AutoRouteProps {
    server: any;
    callbackHandler: any;
    configs?: AutoRoutePropsConfigs;
}

export enum Methods {
    GET = "GET",
    POST = "POST",
    HEAD = "HEAD",
    PUT = "PUT",
    DELETE = "DELETE",
    USE = "USE",
}

export interface Route {
    method: Methods;
    path: string;
    modulePath?: string;
    requestJsonSchema?: {
        body: string;
    };
    responseJsonSchema?: string;
    middleware?: RequestParamHandler[];
}

class AutoRoute<AutoRouteProps> {
    private readonly server: any;
    private readonly callbackHandler: any;
    private readonly configs: AutoRoutePropsConfigs = {};

    constructor(server, callbackHandler, configs) {
        this.server = server;
        this.callbackHandler = callbackHandler;

        let {routeBasePath, routeDirectory} = configs;

        if (routeBasePath) {
            console.warn('routeBasePath has been deprecated, please use routeDirectory');
            routeDirectory = routeDirectory || routeBasePath;
        }

        this.configs = {...configs, routeDirectory};
    }

    async mount(routes: Route[]): Promise<string[]> {
        this.guardParams(routes);

        return Promise.all(routes.map(async route => this.addRoute(route)));
    }

    private guardParams(routes) {
        joi.assert(
            routes,
            joi.array().items(
                joi.object({
                    method: joi.string()
                        .regex(/^GET|POST|HEAD|PUT|DELETE|USE$/)
                        .required(),
                    path: joi.string().min(1),
                    modulePath: joi.string().min(1),
                }).unknown(true),
            ).required(),
        );
    }

    private async addRoute(route) {
        let path;

        try {
            let {method, path: currentPath, middleware = []} = route;
            const {basePath = ""} = this.configs;

            path = join(basePath, currentPath);

            const autoRouteCallback = await this.buildCallback(route);

            if (middleware.length) {
                const middlewareCallbacks: any[] = await this.buildMiddlewareCallbacks(middleware);
                this.server[method.toLowerCase()](path, ...middlewareCallbacks, ...autoRouteCallback);
            } else {
                this.server[method.toLowerCase()](path, ...autoRouteCallback);
            }

            return `${route.method}:${path}: mounted`;
        } catch (error) {
            return `${route.method}:${path} did not mount due to ${error.message}`;
        }
    }

    private async buildCallback(route: Route) {
        let callbacks: ((req: any, res: any, next: any) => any)[] = [];

        if (this.callbackHandler) {
            callbacks = this.callbackHandler.buildCallback(route);
        }

        let {modulePath, path, method} = route;

        if (!modulePath) {
            path = path.split('/').map((piece) =>_.camelCase(piece)).join('/');
            modulePath = `${path}/${method.toLowerCase()}`;
        }

        const callbackModule = await this.buildMiddlewareCallback(modulePath, true);

        if (callbackModule) {
            const callback = (req, res, next) => {
                req.config = {
                    route,
                };

                callbackModule(req, res, next);
            };

            callbacks.push(callback);
        }

        return callbacks;
    }

    private async buildMiddlewareCallbacks(middlewares: string[], isRoute: boolean = false): Promise<any[]> {
        assert(Array.isArray(middlewares), `middleware of type ${typeof middlewares} is not a valid array`);

        return Promise.all(middlewares.map(async middleware => this.buildMiddlewareCallback(middleware), isRoute));
    }

    private async buildMiddlewareCallback(middlewarePath: string, isRoute: boolean = false): Promise<(req: any, res: any, next: () => any) => any> {
        let basePath = ".";

        if (isRoute && this.configs && this.configs.routeDirectory) {
            basePath = this.configs.routeDirectory;
        }

        const fullModulePath = join(basePath, middlewarePath);

        return moduleImporter(fullModulePath);
    }
}

export {
    AutoRoute
};
