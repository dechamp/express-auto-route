import assert from 'assert';
const joi = require("@hapi/joi");

export default class AutoRoutePluginCallback {
    private readonly middlewares: any[];

    constructor(middlewares: any[] = []) {
        joi.assert(middlewares, joi.array().items(
            joi.func()
        ));

        this.middlewares = middlewares;
    }

    buildCallback(route) {
        if (this.middlewares) {
            assert(Array.isArray(this.middlewares), `${typeof this.middlewares} is not a valid array`);

            try {
                return this.middlewares.map(middleware => {
                    return async (...args) => {
                        if (args.length === 3) {
                            args[0].config = {
                                route,
                            };
                        }

                        if (args.length === 4) {
                            args[1].config = {
                                route,
                            };
                        }

                        return await middleware(...args);
                    };
                });
            } catch (error) {
                console.debug(error.message, this.middlewares);
            }
        }

        return [];
    }
}
