import RouteResponseJsonSchemaValidator from "./RouteResponseJsonSchemaValidator";

export default async (req, res, next) => {
  const routeResponseJsonSchemaValidator = new RouteResponseJsonSchemaValidator();

  return await routeResponseJsonSchemaValidator.callback(req, res, next);
};
