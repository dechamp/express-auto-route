import RouteRequestJsonSchemaValidator from "./RouteRequestJsonSchemaValidator";

export default async (req, res, next) => {
  const routeRequestJsonSchemaValidator = new RouteRequestJsonSchemaValidator();

  return await routeRequestJsonSchemaValidator.callback(req, res, next);
};
