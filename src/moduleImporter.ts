import { join, dirname, relative } from "path";

const moduleImporter = async (middlewarePath: string): Promise<any> => {
    try {
        let npmPackageMainDir = "";

        if (process.env && process.env.npm_package_main) {
            npmPackageMainDir = dirname(process.env.npm_package_main);
        }

        const middleWareModule = await import(`${relative(__dirname, join(npmPackageMainDir, middlewarePath))}`);

        return middleWareModule.hasOwnProperty("default") ? middleWareModule.default : middleWareModule;
    } catch (error) {
        console.error(error);
        return false;
    }
};

export default moduleImporter;