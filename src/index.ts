import * as joi from "@hapi/joi";
import { AutoRoute as AutoRouteInitializer, Route, Methods, AutoRoutePropsConfigs } from "./AutoRoute";
import AutoRoutePluginCallback from "./AutoRoutePluginCallback";
import RouteRequestJsonSchemaValidator from "./middleware/routeRequestJsonSchemaValidatorMiddleware";
import RouteResponseJsonSchemaValidator from "./middleware/routeResponseJsonSchemaValidatorMiddleware";

export interface Config extends AutoRoutePropsConfigs{
    server: any;
    routes: Route[];
    middlewares?: any[];
}

const AutoRoute = async (config: Config): Promise<string[]> => {
    joi.assert(
        config,
        joi.object().keys({
            server: joi.any().required(),
            routes: joi.any().required(),
            routeBasePath: joi.string(),
            routeDirectory: joi.string(),
            basePath: joi.string(),
            middlewares: joi.array(),
        }).required(),
    );
    const {server, routes, middlewares, ...routeConfig} = config;
    const autoRoutePluginCallback = new AutoRoutePluginCallback(middlewares);
    const autoRoute = new AutoRouteInitializer(server, autoRoutePluginCallback, routeConfig);

    return await autoRoute.mount(routes);
};

// Backward compatibility
const autoRoute = AutoRoute;

export {
    RouteRequestJsonSchemaValidator,
    RouteResponseJsonSchemaValidator,
    Route,
    Methods,
    AutoRoute,
    autoRoute
};

export default AutoRoute;
