const Ajv = require("ajv");

function buildError(errors) {
    if (errors && errors.length) {
        return errors.map(error => {
            return `${error.dataPath}: ${error.message}` + "\n";
        });
    }

    return false;
}

const jsonSchemaValidationService = (schema, data) => {
    const ajv = new Ajv();
    const responseValidate = ajv.compile(schema);
    responseValidate(data);

    return buildError(responseValidate.errors);
};

export default jsonSchemaValidationService;
