module.exports = {
  extend: "./tsconfig.json",
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json'
    }
  }
}